from setuptools import find_packages, setup

setup(
    name='at-command-interface',
    packages=find_packages(include=['atlib']),
    version='0.1.0',
    description='For serial communication to an ETSI TS 127 007 Universal Mobile Telecommunication System',
    author='Sébastien Barbieri @scips',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    test_suite='tests'
)