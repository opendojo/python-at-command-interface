from atlib import PIN

def test_PIN():
  pin = PIN.PIN()
  assert pin.cpin.check == "AT+CPIN?"
  assert pin.cpin.pin(1234) == "ERROR"
  assert pin.cpin.pin(1111) == "OK"
  assert pin.cpin.response.ready=="READY"