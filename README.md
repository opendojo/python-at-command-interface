# Python At Command Interface

## Description
For serial communication to an ETSI TS 127 007 Universal Mobile Telecommunication System

## Installation
pip3 install at-command-interface

## Usage
To register the SIM card on the network.
To send SMS
To make phone call

## Support
Library is provided as is, no support is provided but you are welcome to raise issue and propose pull requests.

## Roadmap
1. Implement registration
2. Implement SMS sending and receiving
3. Implement making phone call
4. Implement receiving phone call

## Contributing
Feel fork then contribute. 

Please follow the [Latest ETSI TS 127 007](https://www.etsi.org/deliver/etsi_ts/127000_127099/127007/18.06.00_60/ts_127007v180600p.pdf) documentation while contributing and sepcify which section is covered + provide test script.

## Authors and acknowledgment
Thanks to:
- [Sébastien Barbieri](https://gitlab.com/scips)

## License
CC4

## Project status
Just starting