class RESPONSE():
  def __init__(self):
    self.ready = "READY"

class CPIN():
  def __init__(self):
    self.check = "AT+CPIN?"
    self.response = RESPONSE()

  def pin(self, pincode):
    if pincode == 1111:
      return "OK"
    return "ERROR"

class PIN():
  def __init__(self):
    self.cpin = CPIN()


